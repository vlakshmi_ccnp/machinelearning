# author Lakshminarayanan Venugopal
def validate(N,ll,ul):
    retVal = False
    if(N in range(ll,ul+1)):
        retVal = True
    return retVal

def validate_list(ln,ll,ul):
    retVal = False
    for i in ln:
        if(validate(i,ll,ul)):
            retVal = True
        else:
            retVal = False
            break
    return retVal

def count_non_zero_ahead(lx):
    n = 0
    size = len(lx)
    for i in range(size-1):
        if(lx[i] > 0):
            n+=1
    return n

def waiting_time(tickets,p):
    n = len(tickets)
    valid_n = validate(n,1,10**5)
    valid_tickets = validate_list(tickets,1,10**9)
    valid_p= validate(p,0,n-1)
    if(valid_n and valid_tickets and valid_p):
        wtime = p+1
        sl = seeker_last_formation(tickets, p)
        sl_size = len(sl)
        if(sl[sl_size-1]==0): # if the ticket seekers wants only one ticket, just return wtime
            return wtime       
        while(sl[sl_size-1]>0):
            wtime+=count_non_zero_ahead(sl)+1
            sl = seeker_last_formation(sl, len(sl)-1)
            sl_size = len(sl)       
    return wtime


def seeker_last_formation(tickets,p):
    seeker_last= []
    size_tickets = len(tickets)
    i=0
    j=size_tickets-1
    pos=p
    while(pos>=0):
        if((tickets[i]-1)>=0):
            seeker_last.append(tickets[i]-1)         
            i+=1
            pos-=1
        else:
            i+=1
            pos-=1
            continue
    
    while(j>p):
        seeker_last.insert(0,tickets[j])
        j-=1
    return seeker_last
        
t1 = [2,6,3,4,5]
p1 = 2

t2 = [1,1,1,1]
p2 = 0

t3=[5,5,2,3]
p3 = 3

t4 = [7,5,4,6,4,3,1]
p4 = 0
p5= 3
p6 = 1

print(waiting_time(t1, p1))
print(waiting_time(t2, p2))
print(waiting_time(t3, p3))
print(waiting_time(t4, p4))
print(waiting_time(t4, p5))
print(waiting_time(t4, p6))
